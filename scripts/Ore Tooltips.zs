//<>.addTooltip(format.aqua("Mining World"));


//----Overworld----

// Coal Ore
<minecraft:coal_ore>.addTooltip(format.aqua("Overworld"));

//Copper
<ThermalFoundation:Ore>.addTooltip(format.aqua("Overworld"));

//Iron
<minecraft:iron_ore>.addTooltip(format.aqua("Overworld"));

//Tin
<ThermalFoundation:Ore:1>.addTooltip(format.aqua("Overworld"));

//Lead
<ThermalFoundation:Ore:3>.addTooltip(format.aqua("Overworld"));

//Diamond
<minecraft:diamond_ore>.addTooltip(format.aqua("Overworld"));

//Yellorite
<BigReactors:YelloriteOre>.addTooltip(format.aqua("Overworld"));

//Aluminum
<TConstruct:SearedBrick:5>.addTooltip(format.aqua("Overworld"));



//----Nether-----

//Lapis
<NetherOres:tile.netherores.ore.0:4>.addTooltip(format.aqua("The Nether"));

//Gold
<NetherOres:tile.netherores.ore.0:2>.addTooltip(format.aqua("The Nether"));

//Redstone
<NetherOres:tile.netherores.ore.0:5>.addTooltip(format.aqua("The Nether"));

//Quartz
<minecraft:quartz_ore>.addTooltip(format.aqua("The Nether"));

//Ardite
<TConstruct:SearedBrick:2>.addTooltip(format.aqua("The Nether"));

//Cobalt
<TConstruct:SearedBrick:1>.addTooltip(format.aqua("The Nether"));

//Pig Iron
<NetherOres:tile.netherores.ore.1:2>.addTooltip(format.aqua("The Nether"));

//Sulfer
<NetherOres:tile.netherores.ore.1:5>.addTooltip(format.aqua("The Nether"));



//----End---- 


//DISABLED
//Amethyst
//<BiomesOPlenty:gemOre>.addTooltip(format.aqua("The End"));

//Eximite
//<Metallurgy:ender.ore>.addTooltip(format.aqua("The End"));

//Meutoite
//<Metallurgy:ender.ore:1>.addTooltip(format.aqua("The End"));



//Deep Dark

//Silver
<ThermalFoundation:Ore:2>.addTooltip(format.aqua("Deep Dark"));

//Ferrous
<ThermalFoundation:Ore:4>.addTooltip(format.aqua("Deep Dark"));

//Adamantine
<Metallurgy:fantasy.ore:13>.addTooltip(format.aqua("Deep Dark"));

//Atlarus
<Metallurgy:fantasy.ore:14>.addTooltip(format.aqua("Deep Dark"));

//Certus Quartz
<appliedenergistics2:tile.OreQuartz>.addTooltip(format.aqua("Deep Dark"));
<appliedenergistics2:tile.OreQuartzCharged>.addTooltip(format.aqua("Deep Dark"));



//----Mining World----

//Osmium
<Mekanism:OreBlock>.addTooltip(format.aqua("Mining World"));

//Mana Infused
<ThermalFoundation:Ore:6>.addTooltip(format.aqua("Mining World"));

//Shiny
<ThermalFoundation:Ore:5>.addTooltip(format.aqua("Mining World"));
